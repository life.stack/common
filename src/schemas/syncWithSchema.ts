// https://github.com/colinhacks/zod/blob/eda186c7645d66e647fb9866023fb7a96d38fb7d/INTEGRATIONS.MD
import { z } from 'zod'
import { Document } from 'mongoose'

const documentKeys = [
  ...Object.getOwnPropertyNames(Document.prototype),
  //These were added manually; doesn't seem like there are more keys that the prototype misses
  '$__',
  'isNew',
  'errors',
  '$locals',
  '$op',
  '_doc',
]

type NonDocumentKey<T> = keyof Omit<T, keyof typeof Document.prototype>

/**
 * Retrieves all of the keys (property *names*) of the target object, which do not belong
 * to the Document class (i.e. all non-document property keys of the object).
 * @param target the object to extract keys from.
 */
function getNonDocumentKeys<T extends Document>(
  target: T
): NonDocumentKey<T>[] {
  const targetKeys = Object.getOwnPropertyNames(target)

  return targetKeys.filter(
    (key) => !documentKeys.includes(key)
  ) as NonDocumentKey<T>[]
}

/**
 * Returns a *function* used for mongoose middleware validation.
 * Ideally, we could use Zod's parsing methods directly in the mongoose middleware, but since it
 * does not have the option to *return* the validated object, and instead provides it as the `this` parameter,
 * we need to iterate over `this`, after having parsed the relevant fields, assigning fields existing in the parsed response
 * and deleting those that aren't.
 *
 * @param schema the schema to parse incoming objects by.
 */
function syncWithSchema<T>(schema: z.Schema<T>) {
  return async function (this: Document & T) {
    const modelKeys = getNonDocumentKeys(this)

    const modelFields = pick(this, modelKeys)

    /**
     * Of course, you may use `safeParseAsync` or a synchronous variant;
     * The reasoning for using parseAsync here is to support async schemas, as
     * well as throw any error *out* to be captured by the caller of the Mongoose
     * operation (e.g. insertOne).
     */
    const model = await schema.parseAsync(modelFields)

    //Sync up `this` with the parsed `model`.
    modelKeys.forEach((key: keyof T) => {
      if (key in model) {
        this[key] = model[key] as any
      } else {
        delete this[key]
      }
    })
  }
}

function pick<T extends {}, K extends keyof T>(o: T, keys: K[]): Pick<T, K> {
  return keys.reduce(
    (acc, key) => ({ ...acc, [key]: o[key] }),
    {} as Pick<T, K>
  )
}

export default syncWithSchema
