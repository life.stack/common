import { z } from 'zod'

export const UserProfileSchema = z.object({
  displayName: z.string().min(1).max(128).optional(),
  firstName: z.string().min(1).max(128).optional(),
  middleName: z.string().optional(),
  lastName: z.string().optional(),
  birthdate: z.date().optional(),
  avatar: z
    .string()
    .max(100000)
    .regex(/^data:image.+?;base64.+/, 'Avatar must be an image')
    .optional(),
  address: z
    .object({
      street: z.string().optional(),
      city: z.string().optional(),
      state: z.string().optional(),
      postalCode: z.string().optional(),
      country: z.string().optional(),
    })
    .optional(),
})

export interface UserProfile extends z.infer<typeof UserProfileSchema> {}
