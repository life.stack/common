import { NextFunction, Request, RequestHandler, Response } from 'express'
import { ZodSchema } from 'zod'

type ValidatedMiddleware<TBody, TQuery, TParams> = (
  req: Request<TParams, any, TBody, TQuery>,
  res: Response,
  next: NextFunction
) => any

type SchemaDefinition<TBody, TQuery, TParams> = Partial<{
  body: ZodSchema<TBody>
  query: ZodSchema<TQuery>
  params: ZodSchema<TParams>
}>

const check = <TType>(obj?: any, schema?: ZodSchema<TType>): obj is TType => {
  if (!schema) return true
  return schema.safeParse(obj).success
}

export const validate = <TBody = unknown, TQuery = unknown, TParams = unknown>(
  schema: SchemaDefinition<TBody, TQuery, TParams>,
  middleware: ValidatedMiddleware<TBody, TQuery, TParams>,
  path?: boolean
): RequestHandler => {
  return async (req, res, next) => {
    if (schema.body) {
      req.body = schema.body.parse(
        req.body,
        path ? { path: ['body'] } : undefined
      )
    }

    if (schema.query) {
      schema.query.parse(req.query, path ? { path: ['query'] } : undefined)
    }

    if (schema.params) {
      schema.params.parse(req.params, path ? { path: ['params'] } : undefined)
    }

    try {
      return await middleware(
        req as unknown as Request<TParams, any, TBody, TQuery>,
        res,
        next
      )
    } catch (err) {
      next(err)
    }
  }
}

export const validateBody = <TBody>(
  body: ZodSchema<TBody>,
  middleware: ValidatedMiddleware<TBody, unknown, unknown>
) => validate({ body }, middleware, false)

export const validateQuery = <TQuery>(
  query: ZodSchema<TQuery>,
  middleware: ValidatedMiddleware<unknown, TQuery, unknown>
) => validate({ query }, middleware, false)

export const validateParams = <TParams>(
  params: ZodSchema<TParams>,
  middleware: ValidatedMiddleware<unknown, unknown, TParams>
) => validate({ params }, middleware, false)

// export const validate2 = <TBody = unknown, TQuery = unknown, TParams = unknown>(
//   schema: SchemaDefinition<TBody, TQuery, TParams>,
//   middleware: ValidatedMiddleware<TBody, TQuery, TParams>,
//   path?: boolean
// ): RequestHandler => {
//   return async (req, res, next) => {
//     if (
//       check(req.body, schema.body) &&
//       check(req.query, schema.query) &&
//       check(req.params, schema.params)
//     ) {
//       try {
//         return await middleware(
//           req as unknown as Request<TParams, any, TBody, TQuery>,
//           res,
//           next
//         )
//       } catch (err) {
//         next(err)
//       }
//     }
//
//     if (schema.body) {
//       const result = schema.body.safeParse(
//         req.body,
//         path ? { path: ['body'] } : undefined
//       )
//       console.log('1', result)
//       if (!result.success) return next(result.error)
//       req.body = schema.body.parse(req.body)
//       console.log('2', req.body)
//     }
//
//     if (schema.query) {
//       const result = schema.query.safeParse(
//         req.query,
//         path ? { path: ['query'] } : undefined
//       )
//       if (!result.success) return next(result.error)
//     }
//
//     if (schema.params) {
//       const result = schema.params.safeParse(
//         req.params,
//         path ? { path: ['params'] } : undefined
//       )
//       if (!result.success) return next(result.error)
//     }
//
//     return next(new Error('Unable to validate this request'))
//   }
// }
