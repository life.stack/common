import { Request, Response, NextFunction } from 'express'
import { NotAuthorizedError } from '../errors/notAuthorizedError'

export const requireAuth = (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (!req.currentUser || req.currentUser.groups.length === 0) {
    throw new NotAuthorizedError()
  }

  next()
}
