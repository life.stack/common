import { NextFunction, Request, Response } from 'express'
import { ZodError } from 'zod'
import { CustomError } from '../errors/customError'

export const errorHandler = (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  if (process.env.NODE_ENV !== 'test' && err instanceof Error) {
    console.error(err.stack)
  }

  if (err instanceof ZodError) {
    // https://github.com/colinhacks/zod/blob/master/ERROR_HANDLING.md
    return res.status(400).send(
      err.issues.map((issue) => ({
        field: issue.path.join('.'),
        message: issue.message,
        details: issue,
      }))
    )
  }

  if (err instanceof CustomError) {
    return res.status(err.statusCode).send({ errors: err.serializeErrors() })
  }

  console.error(err)

  res.status(400).send({
    errors: [
      {
        message: err.message || 'Something went wrong',
        stackTrace: getStackTrace(err),
      },
    ],
  })
}

const getStackTrace = (err: Error) => {
  if (process.env.NODE_ENV !== 'production') {
    return err.stack
  } else {
    return undefined
  }
}
