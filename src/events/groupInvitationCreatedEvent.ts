import { Subjects } from './subjects'

export interface GroupInvitationCreatedEvent {
  subject: Subjects.GroupInvitationCreated
  data: {
    id: string
    expiresAt: string
  }
}
