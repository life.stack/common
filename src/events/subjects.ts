export enum Subjects {
  // Examples
  TicketCreated = 'ticket:created',
  TicketUpdated = 'ticket:updated',
  OrderCreated = 'order:created',
  OrderCancelled = 'order:cancelled',
  ExpirationComplete = 'expiration:complete',
  PaymentCreated = 'payment:created',
  // Life.stack
  GroupInvitationCreated = 'group:invitation:created',
  GroupInvitationExpired = 'group:invitation:expired',
}
