export enum OrderStatus {
  // When the order has been created; however, the ticket has not been reserved yet
  Created = 'Created',

  // The ticket that is being ordered has already been reserved, cancelled by the user, or the order expires before payment
  Cancelled = 'Cancelled',

  // The order has successfully reserved the ticket
  AwaitingPayment = 'AwaitingPayment',

  // The order has been reserved and the user has provided payment
  Complete = 'Complete',
}
