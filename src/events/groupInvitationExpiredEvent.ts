import { Subjects } from './subjects'

export interface GroupInvitationExpiredEvent {
  subject: Subjects.GroupInvitationExpired
  data: {
    invitationId: string
  }
}
