export abstract class CustomError extends Error {
  abstract statusCode: number

  constructor(public message: string) {
    super(message)

    // https://github.com/Microsoft/TypeScript-wiki/blob/master/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
    Object.setPrototypeOf(this, CustomError.prototype)
  }

  abstract serializeErrors(): {
    message: string
    field?: string
  }[]
}
